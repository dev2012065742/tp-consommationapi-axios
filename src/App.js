// import React, { useEffect, useState } from "react";
// import User from "./User";
// import "./App.css";
// import axios from "axios";

// export default function App() {
//   const [users, setUsers] = useState([]);

//   useEffect(() => {
//     const gettingUsers = async () => {
//       const response = await axios
//         .get("https://jsonplaceholder.typicode.com/users")
//         .then((res) => {
//           console.log(res);
//           setUsers(res.data);
//           console.log("Users :", users);
//         });
//     };

//     gettingUsers();
//   }, []);
//   return (
//     <div className="container">
//       <h1>nombre d'utilisateurs: {users.length}</h1>
//       <div className="sub-container">
//         {users.map((user, index) => (
//           <User user={user} index={index} />
//         ))}
//       </div>
//     </div>
//   );
// }

// *********************************************************************************

import React, { Component } from "react";
import User from "./User";
import "./App.css";
import axios from "axios";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
    };
  }
  componentDidMount() {
    axios.get("https://jsonplaceholder.typicode.com/users").then((res) => {
      console.log(res);
      this.setState({ users: res.data });
      console.log("Users :", this.state.users);
    });
  }

  render() {
    return (
      <div className="container">
        <h1>nombre d'utilisateurs: {this.state.users.length}</h1>
        <div className="sub-container">
          {this.state.users.map((user, index) => (
            <User user={user} index={index} />
          ))}
        </div>
      </div>
    );
  }
}
