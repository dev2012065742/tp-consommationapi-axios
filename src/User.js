// import React, { useState, useEffect } from "react";
// import Post from "./Post";
// import axios from "axios";

// export default function User({ user, index }) {
//   const [showPosts, setShowPosts] = useState(false);
//   const [posts, setPosts] = useState([]);
//   useEffect(() => {
//     const gettingPosts = async () => {
//       const response = await axios
//         .get("https://jsonplaceholder.typicode.com/posts")
//         .then((res) => {
//           setPosts(res.data);
//         });
//     };
//     console.log("Posts :", posts);
//     gettingPosts();
//   }, []);
//   const handleShowPosts = () => {
//     showPosts ? setShowPosts(false) : setShowPosts(true);
//   };
//   const postsPerUser = (id) => {
//     const x = posts.filter((post) => post.userId === id && post);
//     console.log("x", x);
//     return x.length;
//   };

//   return (
//     <div className="user" id={index}>
//       <p>
//         Nom: {user.name}
//         <br />
//         Email: {user.email}
//         <br />
//         Ville : {user.address.city}
//         <br />
//         <button onClick={handleShowPosts}>
//           {showPosts ? "Hide Posts" : "Show Posts"}
//         </button>
//       </p>
//       {showPosts && `nombre des postes: ${postsPerUser(user.id)}`}
//       {showPosts && (
//         <div className="posts">
//           {posts.map(
//             (post) =>
//               post.userId === user.id && <Post post={post} index={index} />
//           )}
//         </div>
//       )}
//     </div>
//   );
// }

//*******************************************************************************

import React, { Component } from "react";
import Post from "./Post";
import axios from "axios";

export default class User extends Component {
  state = {
    showPosts: false,
    posts: [],
  };

  componentDidMount() {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((res) => {
      this.setState({
        posts: res.data,
      });
    });
  }

  handleShowPosts = () => {
    this.setState((prevState) => ({
      showPosts: !prevState.showPosts,
    }));
  };

  postsPerUser = (id) => {
    return this.state.posts.filter((post) => post.userId === id).length;
  };

  render() {
    const { user, index } = this.props;
    const { showPosts, posts } = this.state;

    return (
      <div className="user" id={index}>
        <p>
          Nom: {user.name}
          <br />
          Email: {user.email}
          <br />
          Ville: {user.address.city}
          <br />
          <button onClick={this.handleShowPosts}>
            {showPosts ? "Hide Posts" : "Show Posts"}
          </button>
        </p>

        {showPosts && (
          <div>
            nombre de postes: {this.postsPerUser(user.id)}
            <div className="posts">
              {posts.map(
                (post) =>
                  post.userId === user.id && <Post post={post} index={index} />
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}
